from flask import Blueprint, Response, jsonify, request, render_template
from annoy import AnnoyIndex
import pickle
from pymongo import MongoClient
import json
from bson.objectid import ObjectId 
import requests
import numpy as np


gallery = Blueprint('gallery', __name__, template_folder='templates')

# Connect to database
client = MongoClient('localhost', 27017)
db = client.images
boots = db.boots

@gallery.route('/gallery')
def listRandom():
    result = boots.aggregate([{'$sample': {'size': 60}}])
    return render_template('gallery.html', items=result)

@gallery.route('/gallery/<objectId>')
def listSimilar(objectId):

    # Get ids
    apiUrl = request.host_url + 'similarity/' + objectId
    response = requests.get(apiUrl, headers={'Content-Type': 'application/json'})

    items = json.loads(response.content.decode('utf-8'))
    items_dict = {x['id']: x for x in items}
    objectIds = map(lambda x: ObjectId(x), items_dict.keys())

    # Load labels
    labels = np.load('../labels.npy')

    # Get item data
    result = list(boots.find({'_id': {'$in': list(objectIds)}}))
    for res in result:
        res['distance'] = items_dict[str(res['_id'])]['distance']
        res['embedding'] = sorted(list(zip(labels, res['vgg'])), key=lambda x: x[1], reverse=True)

    result.sort(key=lambda x: x['distance'], reverse=False)

    print(request.args.get('info'))   

    # Render template
    return render_template('gallery-item.html', items=result, showInfo=(True if request.args.get('info') else False))

