from flask import Flask
from .similarity import similarity
from .items import items
from .gallery import gallery
from .data import data

app = Flask(__name__)
app.register_blueprint(similarity)
app.register_blueprint(items)
app.register_blueprint(gallery)
app.register_blueprint(data)
