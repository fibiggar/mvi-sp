from flask import Blueprint, Response, jsonify, request
from annoy import AnnoyIndex
import pickle
from pymongo import MongoClient
import json
from bson.objectid import ObjectId 

items = Blueprint('items', __name__, template_folder='templates')

# Connect to database
client = MongoClient('localhost', 27017)
db = client.images
boots = db.boots

@items.route('/items', methods = ['POST'])
def getItems():
    ids = request.get_json(silent=True)
    objectIds = map(lambda x: ObjectId(x), ids)

    result = boots.find({'_id': {'$in': list(objectIds)}})
    transformed = map(lambda x: {
        '_id': str(x['_id']),
        'path': x['path']
    }, result)
    return Response(json.dumps(list(transformed)),  mimetype='application/json')

