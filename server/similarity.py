from flask import Blueprint, Response, jsonify
from annoy import AnnoyIndex
import pickle
from pymongo import MongoClient
import json

similarity = Blueprint('similarity', __name__, template_folder='templates')

# Load index metadata
with open('../index-meta.pickle', 'rb') as handle:
    indexMeta = pickle.load(handle)

# Load index
f = 1000
index = AnnoyIndex(f)
index.load('../index.ann') # super fast, will just mmap the file

def getSimilarItems(objectId, n):
    itemId = indexMeta['itemsByObjectId'][objectId]
    ids, distances = index.get_nns_by_item(itemId, n, include_distances=True)
    
    ret = []
    for i, sId in enumerate(ids):
        ret.append({
          'id': indexMeta['itemsById'][sId], 
          'distance': distances[i]
        })
    return ret

@similarity.route('/similarity/<objectId>')
def show(objectId):
    similarItems = getSimilarItems(objectId, 60)
    return Response(json.dumps(similarItems), mimetype='application/json')

