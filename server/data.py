from flask import Blueprint, Response, jsonify, request, send_from_directory
from annoy import AnnoyIndex
import pickle
from pymongo import MongoClient
import json
from bson.objectid import ObjectId 

data = Blueprint('data', __name__, template_folder='templates')

@data.route('/data/<path:path>', methods = ['GET'])
def get(path):
    return send_from_directory('/var/data', path)
